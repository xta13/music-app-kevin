var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser')
var models = require('./models');
var mu=require('mu2');
var crypto = require('crypto');
var cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');

mu.root=__dirname;
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({   // to support URL-encoded bodies
    extended: true
}));
app.use(cookieParser())

var server = require('http').Server(app);
var io = require('socket.io')(server);
// var EventEmitter = require('events').EventEmitter;
// //long polling
// var userHasLatest = {};
// var messageBus = new EventEmitter();
// messageBus.setMaxListeners(100);
// messageBus.on('add', function(data) { console.log('added!')})
//
// messageBus.trigger('add', 123);
//
// messageBus.trigger('add', 456);
//
// messageBus.trigger('add', 789);

function contain(arr, element){

  for(var i=0;i<arr.length;i++)
  {
    if (arr[i]===element)
    {
      return true;
    }
  }
  return false;
}

app.get('/library', function(request, response) {
  var sessionKey=request.cookies.sessionKey;
  if(sessionKey != null){
    response.sendFile(__dirname + '/playlist.html');
  }
  else{
    response.status(301);
    response.setHeader('Location', 'http://localhost:3000/login');
    response.send('redirect to login');
  }
});

app.get('/playlists', function(request, response) {
  var sessionKey=request.cookies.sessionKey;
  if(sessionKey != null){
    response.sendFile(__dirname + '/playlist.html');
  }
  else{
    response.status(301);
    response.setHeader('Location', 'http://localhost:3000/login');
    response.send('redirect to login');
  }
});

app.get('/search', function(request, response) {
  var sessionKey=request.cookies.sessionKey;
  if(sessionKey != null){
    response.sendFile(__dirname + '/playlist.html');
  }
  else{
    response.status(301);
    response.setHeader('Location', 'http://localhost:3000/login');
    response.send('redirect to login');
  }
});

app.get('/', function(request, response) {
  var sessionKey=request.cookies.sessionKey;
  if(sessionKey != null){
    response.status(301);
    response.setHeader('Location', 'http://localhost:3000/playlists');
    response.send('redirect to playlists');
  }
  else{
    response.status(301);
    response.setHeader('Location', 'http://localhost:3000/login');
    response.send('redirect to login');
  }
});

app.get('/playlist.css', function(request, response) {
  response.sendFile(__dirname +'/playlist.css');
});

app.get('/music-app.js', function(request, response) {
  response.sendFile(__dirname +'/music-app.js');
});

app.get('/grey.jpg', function(request, response) {
  response.sendFile( __dirname +'/grey.jpg');
});

app.get('/login', function(request, response) {
  response.sendFile( __dirname +'/login.html');
});

var generateKey = function() {
    var sha = crypto.createHash('sha256');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

app.post('/login', function(request, response) {
  // console.log("-----------------------------------------------------"+request.body.username);
  var username=request.body.username;
  var password=request.body.password;
  models.User.findOne(
    {
      where:
        {
          username:username
        }
    })
    .then(function (user){
            // console.log("--------------------------------------"+user.password);
            if( user != null ){
              bcrypt.compare(password, user.password,
                function(err, res) {
                  if(res) {
                   // Passwords match
                     //Create a new session entry
                    var key = generateKey();
                    console.log(key);
                    models.Session.create(
                      {
                        sessionUser : user.id,
                        sessionKey : key
                      });
                    response.status(301);
                    response.setHeader('Set-Cookie', 'sessionKey=' + key);
                    response.setHeader('Location', 'http://localhost:3000/playlists');
                    response.send('login successfully');
                  }
                  else {
                   // Passwords don't match
                   response.status(401);
                   response.send("wrong password");
                  }
              });
            }
            else {
              response.status(401);
              response.send("user not exist");
            }
          })

});

app.get('/api/users',function(req, res){
    models.User.findAll({attributes:['username','id']})
         .then(function(users){
           var result={users:null};
           result.users=users.map(function(user){
               return user.get({plain: true})});
           for(var i=0;i<result.users.length;i++)
           {
             result.users[i].id-=1;
           }
           res.end(JSON.stringify(result));
         })
});

app.get('/api/songs', function(request, response) {
    models.Song.findAll({attributes:['album','duration','title','id','artist']})
        .then(function(songs) {
            var result={songs:null};
            result.songs=songs.map(function(song){
                return song.get({plain: true})});
            for(var i=0;i<result.songs.length;i++)
            {
              result.songs[i].id-=1;
            }
            response.end(JSON.stringify(result));
        })
});


app.get('/api/playlists',function(request, response, callback) {
    var sessionKey=request.cookies.sessionKey;
    models.Playlist.findAll({attributes:['id','name']}).then(
    function(playlists){
          var send={playlists:null};
          send.playlists = playlists.map(function(playlist){
                                        return playlist.get({plain: true
                                    })});
          for(var i=0;i<send.playlists.length;i++)
          {
            send.playlists[i].songs=[];
          }
          var counter=0;
          playlists.forEach(
          function(playlist){
            playlist.getSongs().then(
            function(result){
              for(var i=0;i<send.playlists.length;i++) {
                  if(result.length!=0){
                    if(send.playlists[i].id===result[0].SongsPlaylists.PlaylistId)
                    {
                      for(var j=0;j<result.length;j++)
                      {
                        send.playlists[i].songs.push(result[j].SongsPlaylists.SongId-1)
                      }
                    }
                }
              }
              if(counter===send.playlists.length-1) {
                for(var i=0;i<send.playlists.length;i++)
                {
                    send.playlists[i].id-=1;
                }
                models.Session.findOne({
                  where:
                  {
                    sessionKey:sessionKey
                  }
                })
                .then(function(session){
                    if(session === null) {
                      response.status(403);
                      response.send("session not exist");
                    }
                    else {
                      var user_id = session.sessionUser;
                      models.User.findOne({
                        where:
                        {
                          id:user_id
                        }
                      })
                      .then(function(user) {
                            var result = user.getPlaylists()
                                        .then(function (playlists_for_user){
                                                    //console.log("plllllllllllllllllllllllllllllllllllllllllll"+playlists);
                                                    var send2={playlists:[]};
                                                    for (var i = 0; i < playlists_for_user.length; i++){
                                                        // console.log("playlists_for_user"+i+JSON.stringify(playlists_for_user[i]))
                                                        for(var j=0; j < send.playlists.length;j++){
                                                          // console.log("send.playlists"+j+JSON.stringify(send.playlists[j]))
                                                            if(send.playlists[j].id === playlists_for_user[i].id-1){
                                                              send2.playlists.push(send.playlists[j]);
                                                            }
                                                        }
                                                    }
                                                    console.log("-----------------------------------"+JSON.stringify(send2))
                                                    response.end(JSON.stringify(send2));
                                              });
                      })
                  }
                })
              }
              counter++;
            })
          }

          );
              console.log("hhhhhhhhhhhhhhhhhhhhhhhhhhell-------------------------------------------------------------------o"+JSON.stringify(send));
            })
          }
)
io.on('connection', function(socket) {
  app.post('/api/playlists/:id', function(request, response) {
      console.log(request.body);
      var id=parseInt(request.params.id)+1;
      var song=parseInt(request.body.song)+1;
      var sessionKey=request.cookies.sessionKey;
      models.Session.findOne({
        where:
        {
          sessionKey:sessionKey
        }
      })
      .then(function(session){
          if(session === null) {
            response.status(403);
            response.send("session not exist");
          }
          else {
            var user_id = session.sessionUser;
            models.User.findOne({
              where:
              {
                id:user_id
              }
            })
            .then( function(user) {
                    var result = user.getPlaylists()
                                .then(function (playlists){
                                            //console.log("plllllllllllllllllllllllllllllllllllllllllll"+playlists);
                                               var playlists_id=[];
                                               for(var i=0; i<playlists.length;i++)
                                               {
                                                 playlists_id.push(playlists[i].id);
                                               }
                                               if (!contain(playlists_id,id)) {
                                                 response.status(403);
                                                 response.send("you do not have permission to add song into this playlist");
                                               }
                                               else {
                                                  models.Playlist.findOne(
                                                  {
                                                     where:
                                                       {
                                                         id:id
                                                       }
                                                  })
                                                  .then(function(playlist) {
                                                                  playlist.addSong(song);
                                                                  var send={playlist:playlist.id-1,song:song-1};
                                                                  socket.broadcast.emit('addedSongsForPlaylist', JSON.stringify(send));
                                                                  response.end("successfully added song into playlist");
                                                  })
                                               }
                                      });
            })
        }
      })
  });


  app.delete('/playlists/:id', function(request, response) {
      console.log(request.body);
      var id=parseInt(request.params.id)+1;
      var song=parseInt(request.body.song)+1;
      var sessionKey=request.cookies.sessionKey;
      models.Session.findOne({
        where:
        {
          sessionKey:sessionKey
        }
      })
      .then(function(session){
          if(session === null) {
            response.status(403);
            response.send("session not exist");
          }
          else {
            var user_id = session.sessionUser;
            models.User.findOne({
              where:
              {
                id:user_id
              }
            })
            .then( function(user) {
                    var result = user.getPlaylists()
                                .then(function (playlists){
                                            //console.log("plllllllllllllllllllllllllllllllllllllllllll"+playlists);
                                               var playlists_id=[];
                                               for(var i=0; i<playlists.length;i++)
                                               {
                                                 playlists_id.push(playlists[i].id);
                                                 console.log("user "+user_id+" ------------------------------add"+playlists[i].id);
                                               }
                                               if (!contain(playlists_id,id)) {
                                                 console.log("------------------------id passed is", id);
                                                 response.status(403);
                                                 response.send("you do not have permission to delete song from this playlist");
                                               }
                                               else {
                                                 models.Playlist.findOne(
                                                   {
                                                     where:
                                                       {
                                                         id:id
                                                       }
                                                   })

                                                     .then(function(playlist) {

                                                         playlist.removeSong(song);
                                                         var send={playlist:playlist.id-1,song:song-1};
                                                         socket.broadcast.emit('deletedSongsForPlaylist', JSON.stringify(send));
                                                         response.end("successfully removed song in playlist");
                                                     })
                                               }
                                      });
            })
        }
      })
  });

});


app.post('/api/playlists/:id/users', function(request,response){
  var playlist_id=parseInt(request.params.id)+1;
  var user_id=parseInt(request.body.user)+1;
  var sessionKey=request.cookies.sessionKey;
  models.User.findOne({
    where:
    {
      id:user_id
    }
  })
  .then(function(user){
    user.addPlaylist(playlist_id);
    response.end("successfully added user for this playlist");
  })
})
app.post('/api/playlists', function(request, response) {
    console.log(request.body);
    var sessionKey=request.cookies.sessionKey;
    var name=request.body.name;
    models.Playlist.create({name: name})
      .then(function(playlist) {
            //grant access for the user who create this playlist
            models.Session.findOne({
              where:
              {
                sessionKey:sessionKey
              }
            })
            .then(function(session){
                var user_id = session.sessionUser;
                models.User.findOne({where:{id:user_id}})
                .then(function(user){
                  user.addPlaylist(playlist.id);
                })
            })
            var result;
            result = {id:null, name:null}
            result.id=playlist.id-1;
            result.name=playlist.name;
            console.log("result is --------------------"+JSON.stringify(result))

            response.end(JSON.stringify(result));
      })
});
models.sequelize.sync().then(function() {
    server.listen(3000, function () {
      console.log('Example app listening on port 3000! Open and accepting connections until someone kills this process');
    });
});
