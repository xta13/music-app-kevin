
window.MUSIC_DATA={ playlists:[], songs:[],users:[] };
function ajax1() {
     return $.getJSON( "./api/songs", function( data ) {
        window.MUSIC_DATA.songs=data.songs;
   });
}
function ajax2(){
      return $.getJSON( "./api/playlists", function( data ) {
         window.MUSIC_DATA.playlists=data.playlists;

      });
}

function ajax3(){
      return $.getJSON( "./api/users", function( data ) {
         window.MUSIC_DATA.users=data.users;

      });
}

$.when(ajax1(),ajax2(),ajax3()).done( function(){
// console.log(MUSIC_DATA.users);
//JSON.parse($.getJSON("songs.json"));
// MUSIC_DATA.songss=JSON.parse( $.getJSON("songs.json"));
var library=document.getElementById("topSelection1");
var c1=document.getElementById("c1");
var playlists=document.getElementById("topSelection2");
var c2=document.getElementById("c2");
var search=document.getElementById("topSelection3");
var c3=document.getElementById("c3")
//get 3 content div
var content1=document.getElementById("content1");
var content2=document.getElementById("content2");
var content2B=document.getElementById("content2B");
var content3=document.getElementById("content3");
//get "sort by artist" and "sort by title"
var title=document.getElementById("sort_by_title");
var artist=document.getElementById("sort_by_artist");

//get modal box
var modal_content=document.getElementById("modal-content");
var current_song;
var current_playlist;
var add_playlist_button=document.getElementById("add_playlist_button");
var form_to_add=document.getElementById("form_to_add");
var input_to_submit=document.getElementById("input_to_submit");
var input_name=document.getElementById("Pname");

var socket = io('/');
var is_triggered_add_user=false;
var is_triggered_delete_user=false;
var find_playlist_index_by_id=function(playlist_id){
  var i=0;
  for(i=0;i<window.MUSIC_DATA.playlists.length;i++){
    if(playlist_id === window.MUSIC_DATA.playlists[i].id){
      return i;
    }
  }
  return i;
}
socket.on('addedSongsForPlaylist', function(data){
  if(is_triggered_add_user){
    console.log("you added a song");
    is_triggered_add_user=false;
  }
  else{
    var song_id=JSON.parse(data).song;
    var playlist_id=JSON.parse(data).playlist;
    var element={};
    var index= find_playlist_index_by_id(playlist_id);
    if(index< window.MUSIC_DATA.playlists.length) {
      console.log("someone has added a song");
      console.log("playlist id is"+ playlist_id);
      console.log("song id is" + song_id);
      window.MUSIC_DATA.playlists[index].songs.push(song_id);
      element.innerHTML=window.MUSIC_DATA.playlists[index].name;
      console.log(element.innerHTML);
      var temp=show_playlist(element);
      temp();
    }
  }
});
var find_song_index_by_id=function(playlist_index,song_id){
  var i=0;
  for(i=0;i<window.MUSIC_DATA.playlists[playlist_index].songs.length;i++){
    if(song_id === window.MUSIC_DATA.playlists[playlist_index].songs[i]){
      return i;
    }
  }
  return i;
}
socket.on('deletedSongsForPlaylist', function(data){
  if(is_triggered_delete_user){
    console.log("you deleted a song");
    is_triggered_delete_user=false;
  }
  else{
    var song_id=JSON.parse(data).song;
    var playlist_id=JSON.parse(data).playlist;
    var element={};
    var index= find_playlist_index_by_id(playlist_id);
    if(index< window.MUSIC_DATA.playlists.length) {
      console.log("someone has deleted a song");
      console.log("playlist id is"+ playlist_id);
      console.log("song id is" + song_id);
      var song_index=find_song_index_by_id(index,song_id);
      if(song_index<window.MUSIC_DATA.playlists[index].songs.length){
        window.MUSIC_DATA.playlists[index].songs.splice(song_index,1);
        element.innerHTML=window.MUSIC_DATA.playlists[index].name;
        console.log(element.innerHTML);
        var temp=show_playlist(element);
        temp();
      }
    }
  }
});

//when click on library
var click_lib=function(){
  // while(!(xmlhttp2.readyState == 4 && xmlhttp2.status == 200))
  // {
  // }
  // change the color of taps
  // $.ajax({url: "./api/playlists", dataType: "json",  success: function(result){
  //   $ (window.MUSIC_DATA.playlists= JSON.parse(this.responseText).playlists);
  // }});
  history.pushState(true,"Exercise 1","/library");
  library.style.color= "#93236c";
  c1.style.color= "#93236c";
  playlists.style.color= "#000000";
  c2.style.color= "#cacaca";
  search.style.color= "#000000";
  c3.style.color= "#cacaca";
  // when click library tab deafult is sort by artist
  var title=document.getElementsByClassName("special_library");
  var artist=document.getElementsByClassName("special1_library");
  var i;
  //change the content if needed
  for(i=0;i<title.length;i++)
  {
    title[i].innerHTML=sortArtist[i].title;
    artist[i].innerHTML=sortArtist[i].artist;
  }
  // change the button style
  var artist_button= document.getElementById("sort_by_artist");
  var title_button=document.getElementById("sort_by_title");
  artist_button.style="box-shadow:inset 0 0 10px #000000;";
  title_button.style= "box-shadow:none;";

  //display proper content
  document.getElementById("content1").style.display="initial";
  document.getElementById("content2B").style.display="none";
  document.getElementById("content2").style.display="none";
  document.getElementById("content3").style.display="none";

  }
  //when click on search
  var click_search=function(){
      // change the color of taps
      history.pushState(true,"Exercise 1","/search");
      search.style.color= "#93236c";
      c3.style.color= "#93236c";

      playlists.style.color= "#000000";
      c2.style.color="#cacaca";
      library.style.color= "#000000";
      c1.style.color="#cacaca";
      //display proper content
      document.getElementById("content3").style.display="initial";
      document.getElementById("content1").style.display="none";
      document.getElementById("content2B").style.display="none";
      document.getElementById("content2").style.display="none";

  }

//call back function to show the modal
var show_box=function(sign,type){
  return function(){
    //set_up_modal();
    var modal=document.getElementById("modal");
    modal.style="display:initial;";
    //only place changing value of "current_song"
    if(type === 1){
      for(var i=0;i<box_content.length;i++){
        box_content[i].style.display="initial";
      }
      for(var i=0;i<box_content2.length;i++){
        box_content2[i].style.display="none";
      }
      current_song=sign.parentNode.firstChild.childNodes[1].innerHTML;
    }
    else if(type === 2) {
      for(var i=0;i<box_content.length;i++){
        box_content[i].style.display="none";
      }
      for(var i=0;i<box_content2.length;i++){
        box_content2[i].style.display="initial";
      }
      current_playlist=sign.parentNode.firstChild.textContent;
    }

  }
}

// var show_box2=function(sign){
//   return function(){
//     //set_up_modal();
//     var modal=document.getElementById("modal");
//     modal.style="display:initial;";
//     //only place changing value of "current_playlist"
//     current_playlist=sign.parentNode.firstChild;
//
//   }
// }
// var add_user_to_playlists=function(){
//
//
//
// }


//call back function to show a playlist
var show_playlist=function(playlist){
    //get the top level node
    return function() {
      var content=document.getElementById("content2B");
      //delete all the oldcontent first
      while(content.hasChildNodes())
      {
        content.removeChild(content.firstChild);
      }

      //set up the h1
      var h1=document.createElement("h1");
      h1.setAttribute('class','playlist-name');
      var text=document.createTextNode(playlist.innerHTML);
      var add_user=document.createElement("button");
      add_user.setAttribute('id','add_user');
      add_user.innerHTML="+User";
      h1.appendChild(text);
      h1.appendChild(add_user);
      add_user.addEventListener('click',show_box(add_user,2),false);
      content.appendChild(h1);
      //set up for rest

      //find the correct(matching) playlist
      for(i=0;i<window.MUSIC_DATA.playlists.length;i++)
      {
        if(playlist.innerHTML===window.MUSIC_DATA.playlists[i].name)
        {
          var ids=window.MUSIC_DATA.playlists[i].songs.slice();
          break;
        }
      }
      var button3;
      for(i=0;i<ids.length;i++)
      {
        //create every new element as needed
        div=document.createElement("div");
        div.setAttribute('class','part_playlist')
        button=document.createElement("button");
        button.setAttribute('type','button');
        button.setAttribute('class','list_playlist');
        button2=document.createElement("button");
        button2.setAttribute('class','glyphicon glyphicon-plus-sign');
        button3=document.createElement("button");
        // button3.setAttribute('id','remove_song');
        button3.setAttribute('class','glyphicon glyphicon-remove');
        img=document.createElement("img");
        img.setAttribute('src','grey.jpg');
        img.setAttribute('alt','grey box');
        span1=document.createElement("span");
        span1.setAttribute('class','special_playlist');
        //name of the current playlist
        buttonName=document.createTextNode(window.MUSIC_DATA.songs[ids[i]].title);
        span2=document.createElement("span");
        span2.setAttribute('class', 'glyphicon glyphicon-play');
        lineBreaker=document.createElement('br');
        span3=document.createElement("span");
        span3.setAttribute('class','special1_playlist');
        buttonNameArtist=document.createTextNode(window.MUSIC_DATA.songs[ids[i]].artist);
        //assign child element according to the relation of these elements
        span1.appendChild(buttonName);
        span3.appendChild(buttonNameArtist);

        button.appendChild(img);
        button.appendChild(span1);
        button.appendChild(span2);
        button.appendChild(lineBreaker);
        button.appendChild(span3);

        div.appendChild(button);
        div.appendChild(button2);
        div.appendChild(button3);

        content.appendChild(div);
        //add eventlistener for +
        button2.addEventListener('click', show_box(button2,1),false);
        button3.addEventListener('click', remove_song_from_playlist(div));
    }

  document.getElementById("content2").style.display="none";
  document.getElementById("content2B").style.display="initial";
  document.getElementById("content1").style.display="none";
  document.getElementById("content3").style.display="none";
  }

  }

//set up sort by title
function compare_by_title(a,b) {
  var temA=a.title;
  var temB=b.title;
  if(a.title.indexOf("The ")==0)
  {
    temA=a.title.substr(4);
  }
  if(b.title.indexOf("The ")==0)
  {
    temB=b.title.substr(4);

  }
  if (temA.toLowerCase() < temB.toLowerCase())
    return -1;
  if (temA.toLowerCase() >temB.toLowerCase())
    return 1;
  return 0;
}

//set up sort by artist
function compare_by_artist(a,b) {
  var temA=a.artist;
  var temB=b.artist;
  if(a.artist.indexOf("The ")==0)
  {
    temA=a.artist.substr(4);
  }
  if(b.artist.indexOf("The ")==0)
  {
    temB=b.artist.substr(4);

  }
  if (temA.toLowerCase()< temB.toLowerCase())
    return -1;
  if (temA.toLowerCase() >temB.toLowerCase())
    return 1;
  return 0;
}
var copy1=window.MUSIC_DATA.songs.slice();
var copy2=window.MUSIC_DATA.songs.slice();
var sortTitle=copy1.sort(compare_by_title);
var sortArtist=copy2.sort(compare_by_artist);
//set up content1
var i;
var div;
var button;
var button2;
var img;
var span1;
var span2;
var span3;
var buttonPlus;
var buttonName;
var buttonNameArtist;
var lineBreaker;
for(i=0;i<window.MUSIC_DATA.songs.length;i++)
{
  //create every new element as needed
  div=document.createElement("div");
  div.setAttribute('class','part')
  button=document.createElement("button");
  button.setAttribute('type','button');
  button.setAttribute('class','list_library');
  button2=document.createElement("button");
  button2.setAttribute('class','glyphicon glyphicon-plus-sign');
  img=document.createElement("img");
  img.setAttribute('src','grey.jpg')
  img.setAttribute('alt','grey box')
  span1=document.createElement("span");
  span1.setAttribute('class','special_library');
  //name of the current playlist
  buttonName=document.createTextNode(sortArtist[i].title);
  span2=document.createElement("span");
  span2.setAttribute('class', 'glyphicon glyphicon-play');
  lineBreaker=document.createElement('br');
  span3=document.createElement("span");
  span3.setAttribute('class','special1_library');
  buttonNameArtist=document.createTextNode(sortArtist[i].artist);
  //assign child element according to the relation of these elements
  span1.appendChild(buttonName);
  span3.appendChild(buttonNameArtist);

  button.appendChild(img);
  button.appendChild(span1);
  button.appendChild(span2);
  button.appendChild(lineBreaker);
  button.appendChild(span3);

  div.appendChild(button);
  div.appendChild(button2);

  content1.appendChild(div);
  //when click + sign, should change background color and show the pop up box
  button2.addEventListener('click',show_box(button2,1),false);
}
//set up content2
for(i=0;i<window.MUSIC_DATA.playlists.length;i++)
{
  //create every new element as needed
  div=document.createElement("div");
  div.setAttribute('class','part1')
  button=document.createElement("button");
  button.setAttribute('type','button');
  button.setAttribute('class','list');
  img=document.createElement("img");
  img.setAttribute('src','grey.jpg')
  img.setAttribute('alt','grey box')
  span1=document.createElement("span");
  span1.setAttribute('class','special');
  //name of the current playlist
  buttonName=document.createTextNode(window.MUSIC_DATA.playlists[i].name);
  span2=document.createElement("span");
  span2.setAttribute('class', 'glyphicon glyphicon-chevron-right');
  //assign child element according to the relation of these elements
  span1.appendChild(buttonName);

  button.appendChild(img);
  button.appendChild(span1);
  button.appendChild(span2);

  div.appendChild(button);
  //add eventlistener for all playlists buttons
  button.addEventListener('click',show_playlist(span1), false);
  content2.appendChild(div);
}
//setup content3(empty)

//set up playlists content in the modal box
  var p=[];
  var text;
  for(i=0;i<window.MUSIC_DATA.playlists.length;i++)
  {
    p[i]=document.createElement("p");
    p[i].setAttribute('class','playlists-in-box');
    text=document.createTextNode(window.MUSIC_DATA.playlists[i].name);
    p[i].appendChild(text);
    modal_content.appendChild(p[i]);
  }
//set up users content in the modal box
  var user=[];
  for(i=0;i<window.MUSIC_DATA.users.length;i++)
  {
    p[i]=document.createElement("p");
    p[i].setAttribute('class','users-in-box');
    text=document.createTextNode(window.MUSIC_DATA.users[i].username);
    p[i].appendChild(text);
    modal_content.appendChild(p[i]);
  }





// function set_up_modal(){
//   var p=[];
//   var text;
//   while(modal_content.hasChildNodes())
//   {
//     modal_content.removeChild(modal_content.firstChild);
//   }
//   for(i=0;i<window.MUSIC_DATA.playlists.length;i++)
//   {
//     p[i]=document.createElement("p");
//     p[i].setAttribute('class','playlists-in-box');
//     text=document.createTextNode(window.MUSIC_DATA.playlists[i].name);
//     p[i].appendChild(text);
//     modal_content.appendChild(p[i]);
//   }
// }
//when click on playlists tap
var display_playlist=function()
{
  // change the color of taps
  playlists.style.color= "#93236c";
  c2.style.color= "#93236c";
  library.style.color= "#000000";
  c1.style.color= "#cacaca";
  search.style.color= "#000000";
  c3.style.color="#cacaca";
  //display proper content
  history.pushState(true,"Exercise 1","/playlists");
  document.getElementById("content2").style.display="initial";
  document.getElementById("content2B").style.display="none";
  document.getElementById("content1").style.display="none";
  document.getElementById("content3").style.display="none";
}

playlists.onclick=display_playlist;

library.onclick=click_lib;
// when click on sort_by_artist
artist.onclick=function(){
 var title=document.getElementsByClassName("special_library");
 var artist=document.getElementsByClassName("special1_library");
 var i;
 //change the content if needed
 for(i=0;i<title.length;i++)
 {
   title[i].innerHTML=sortArtist[i].title;
   artist[i].innerHTML=sortArtist[i].artist;
 }
 // change the button style
 var artist_button= document.getElementById("sort_by_artist");
 var title_button=document.getElementById("sort_by_title");
 artist_button.style="box-shadow:inset 0 0 10px #000000;";
 title_button.style= "box-shadow:none;";

}
// //when click on sort_by_title
title.onclick=function(){
  var title=document.getElementsByClassName("special_library");
  var artist=document.getElementsByClassName("special1_library");
  var i;
  //change the content if needed
  for(i=0;i<title.length;i++)
  {
    title[i].innerHTML=sortTitle[i].title;
    artist[i].innerHTML=sortTitle[i].artist;
  }
  // change the button style
  var artist_button= document.getElementById("sort_by_artist");
  var title_button=document.getElementById("sort_by_title");
  title_button.style="box-shadow:inset 0 0 10px #000000;";
  artist_button.style= "box-shadow:none;";
}
//when click x, should "close" the box
var close_button=document.getElementById("close");
var box_content=document.getElementsByClassName("playlists-in-box");
var box_content2=document.getElementsByClassName("users-in-box");
//for x sign
var close_box=function(){
    var modal_close=document.getElementById("modal");
    modal_close.style="display:none;";
}
function contain(arr, element){

  for(var i=0;i<arr.length;i++)
  {
    if (arr[i]===element)
    {
      return true;
    }
  }
  return false;
}

// for modal content:playlists
var add_and_close_box=function(playlist_name){
  return function(){

      for (i=0;i<window.MUSIC_DATA.songs.length;i++)
      {
        if (current_song===window.MUSIC_DATA.songs[i].title)
        {

            var j;
            for(j=0;j<window.MUSIC_DATA.playlists.length;j++)
            {
              if(playlist_name===window.MUSIC_DATA.playlists[j].name && !contain(window.MUSIC_DATA.playlists[j].songs,window.MUSIC_DATA.songs[i].id))
              {
                window.MUSIC_DATA.playlists[j].songs.push(window.MUSIC_DATA.songs[i].id);
                //sending ajax
                var index=window.MUSIC_DATA.playlists[j].songs.length-1;
                var data={song:null};
                data.song=window.MUSIC_DATA.playlists[j].songs[index];
                var id =window.MUSIC_DATA.playlists[j].id.toString();
                is_triggered_add_user=true;
                $.ajax({
                type: "POST",
                url: 'api/playlists/'+id,
                data: data,
                dataType: 'application/json'
                });
                break;
              }
            }
            break;
        }
      }
      var modal_close=document.getElementById("modal");
      modal_close.style="display:none;";


   }
}

// for modal content:users
var add_and_close_box2=function(user_name){
  return function(){
      for (i=0;i<window.MUSIC_DATA.playlists.length;i++)
      {
        if (current_playlist===window.MUSIC_DATA.playlists[i].name)
        {

            var j;
            for(j=0;j<window.MUSIC_DATA.users.length;j++)
            {
              if(user_name===window.MUSIC_DATA.users[j].username)
              {

                //sending ajax
                var data={user:null};
                data.user=window.MUSIC_DATA.users[j].id;
                var playlist_id =window.MUSIC_DATA.playlists[i].id.toString();

                $.ajax({
                type: "POST",
                url: 'api/playlists/'+playlist_id+'/users',
                data: data,
                dataType: 'application/json'
                });
                break;
              }
            }
            break;
        }
      }
      var modal_close=document.getElementById("modal");
      modal_close.style="display:none;";


   }
}
close_button.addEventListener('click',close_box,false);

for(i=0;i<box_content.length;i++)
{

  box_content[i].addEventListener('click',add_and_close_box(box_content[i].textContent),false);
}
for(i=0;i<box_content2.length;i++)
{

  box_content2[i].addEventListener('click',add_and_close_box2(box_content2[i].textContent),false);
}

search.onclick=click_search;
//add eventlistener for search bar
var searchBar=document.getElementById("searchBar");
var search_result_section=document.getElementById("search-result");
var searchString;
searchBar.onkeyup = function(event){
  //delete everything except header in content3 each time entered a character
  while(search_result_section.hasChildNodes())
  {
    search_result_section.removeChild(search_result_section.firstChild);
  }
  //get the value from input element
  searchString=searchBar.value.toLowerCase();
  var search_result_playlists=[];
  for(i=0;i<window.MUSIC_DATA.playlists.length;i++)
  {
    var tem =window.MUSIC_DATA.playlists[i].name;
    if((tem.toLowerCase().includes(searchString))&& searchString!=" " &&searchString!="")
    {

      search_result_playlists.push(tem);
    }
  }
  var search_result_songs_title=[];
  var search_result_songs_artist=[];
  for(i=0;i<window.MUSIC_DATA.songs.length;i++)
  {
    var tem_title =window.MUSIC_DATA.songs[i].title;
    var tem_artist =window.MUSIC_DATA.songs[i].artist;
    if((tem_title.toLowerCase().includes(searchString) || tem_artist.toLowerCase().includes(searchString))&& searchString!=" " &&searchString!="")
    {
      search_result_songs_title.push(tem_title);
      search_result_songs_artist.push(tem_artist);
    }
  }
//re-set  the playlist elements according to the input
  //re set playlist
  for(i=0;i<search_result_playlists.length;i++)
  {
    //create every new element as needed
    div=document.createElement("div");
    div.setAttribute('class','part_search')
    button=document.createElement("button");
    button.setAttribute('type','button');
    button.setAttribute('class','list_search');
    img=document.createElement("img");
    img.setAttribute('src','grey.jpg')
    img.setAttribute('alt','grey box')
    span1=document.createElement("span");
    span1.setAttribute('class','special_search');
    //name of the current playlist
    buttonName=document.createTextNode(search_result_playlists[i]);
    span2=document.createElement("span");
    span2.setAttribute('class', 'glyphicon glyphicon-chevron-right');
    //assign child element according to the relation of these elements
    span1.appendChild(buttonName);
    button.appendChild(img);
    button.appendChild(span1);
    button.appendChild(span2);
    button.addEventListener('click', display_playlist,false);
    button.addEventListener('click',show_playlist(span1),false);
    div.appendChild(button);

    //add eventlistener for show a playlist
    // button.addEventListener('click',show_playlist(span1), false);
    search_result_section.appendChild(div);
  }
  //re set song part
  for(i=0;i<search_result_songs_title.length;i++)
  {
    //create every new element as needed
    div=document.createElement("div");
    div.setAttribute('class','part_search')
    button=document.createElement("button");
    button.setAttribute('type','button');
    button.setAttribute('class','list_search_playlist');
    button2=document.createElement("button");
    button2.setAttribute('class','glyphicon glyphicon-plus-sign');
    img=document.createElement("img");
    img.setAttribute('src','grey.jpg')
    img.setAttribute('alt','grey box')
    span1=document.createElement("span");
    span1.setAttribute('class','special_search_playlist');
    //name of the current playlist
    buttonName=document.createTextNode(search_result_songs_title[i]);
    span2=document.createElement("span");
    span2.setAttribute('class', 'glyphicon glyphicon-play');
    lineBreaker=document.createElement('br');
    span3=document.createElement("span");
    span3.setAttribute('class','special1_search_playlist');
    buttonNameArtist=document.createTextNode(search_result_songs_artist[i]);
    //assign child element according to the relation of these elements
    span1.appendChild(buttonName);
    span3.appendChild(buttonNameArtist);
    button.appendChild(img);
    button.appendChild(span1);
    button.appendChild(span2);
    button.appendChild(lineBreaker);
    button.appendChild(span3);
    div.appendChild(button);
    div.appendChild(button2);
    search_result_section.appendChild(div);
    //add eventlistener for +
    button2.addEventListener('click',show_box(button2,1),false);
 }
}
if(window.location.pathname==="/library")
{
  click_lib();
}
else if(window.location.pathname==="/search")
{

  click_search();
}
else if(window.location.pathname==="/playlists")
{
  display_playlist();

}

add_playlist_button.onclick=function(){


  form_to_add.style.display="initial";
  input_name.value="";


}

// when new playlist is added
var add_new_playlist=function(new_playlist){

      div=document.createElement("div");
      div.setAttribute('class','part1')
      button=document.createElement("button");
      button.setAttribute('type','button');
      button.setAttribute('class','list');
      img=document.createElement("img");
      img.setAttribute('src','grey.jpg')
      img.setAttribute('alt','grey box')
      span1=document.createElement("span");
      span1.setAttribute('class','special');
      //name of the current playlist
      buttonName=document.createTextNode(new_playlist.name);
      span2=document.createElement("span");
      span2.setAttribute('class', 'glyphicon glyphicon-chevron-right');
      //assign child element according to the relation of these elements
      span1.appendChild(buttonName);

      button.appendChild(img);
      button.appendChild(span1);
      button.appendChild(span2);

      div.appendChild(button);
      //add eventlistener for all playlists buttons
      button.addEventListener('click',show_playlist(span1), false);
      content2.appendChild(div);

}
// when a remove button is clicked
var remove_song_from_playlist=function(div){
    return function(){
      var index;
      for(index=0;index < window.MUSIC_DATA.playlists.length; index++) {
        if(window.MUSIC_DATA.playlists[index].name === div.parentNode.firstChild.firstChild.textContent) {
          break;
        }

      }
      var id = window.MUSIC_DATA.playlists[index].id ;
      var index_of_song;
      var i;
      var song_id;
      for(i = 0; i < window.MUSIC_DATA.songs.length; i++) {
        if(window.MUSIC_DATA.songs[i].title === div.firstChild.childNodes[1].innerHTML) {
          song_id =window.MUSIC_DATA.songs[i].id ;
          var data = {song : song_id};
          is_triggered_delete_user = true;
          $.ajax({
            url: '/playlists/' + id.toString(),
            type: 'DELETE',
            data: data,
            success: function(data) {

                for(var j=0;j<window.MUSIC_DATA.playlists[index].songs.length;j++) {
                  if(window.MUSIC_DATA.playlists[index].songs[j]===window.MUSIC_DATA.songs[i].id) {
                     window.MUSIC_DATA.playlists[index].songs.splice(j, 1);
                     $(div).remove();

                }
                }
            }
          });
          break;
        }
      }
    }
  }


//adding a new playlist
  $(function() {

      var $form = $('#form_to_add');

      $form.find('input[type="submit"]').click(function() {
          var playlists=document.getElementById("content2").getElementsByClassName("special");
          var value = document.forms["myform"]["playlist-name"].value;
          form_to_add.style.display="none";
          var if_add_needed = true;
          for(var i = 0; i < playlists.length; i++) {
            if (value === playlists[i].textContent) {
              if_add_needed = false;
            }
          }


          //sending ajax
          if(if_add_needed === true){
              var data={name:null};
              data.name=value;
            $.post("api/playlists", data, function(data, status){
              if(status==='success')
              {
                var tem = JSON.parse(data)
                var new_playlist={id:tem.id, name:tem.name,songs:[]};
                window.MUSIC_DATA.playlists.push(new_playlist);
                add_new_playlist(tem);
                //add new content in modal
                var new_playlist_in_modal=document.createElement("p");
                new_playlist_in_modal.setAttribute('class','playlists-in-box');
                var text=document.createTextNode(tem.name);
                new_playlist_in_modal.appendChild(text);
                modal_content.appendChild(new_playlist_in_modal);
                new_playlist_in_modal.addEventListener('click',add_and_close_box(new_playlist_in_modal.textContent),false);



              }
            });
          }
         return false;
      });
  });
});
