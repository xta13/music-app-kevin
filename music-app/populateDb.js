var fs = require('fs');
var models = require('./models');
const bcrypt = require('bcrypt');

function create_users(){
  bcrypt.hash('123', 10, function(err, hash) {
    models.User.create({username: "Foo", password: hash})
    .then(function(user) {
        user.addPlaylist(1);
        user.addPlaylist(3);
    });
  });
  bcrypt.hash('456', 10, function(err, hash) {
    models.User.create({username: "Bar", password: hash})
    .then(function(user) {
        user.addPlaylist(2);
        user.addPlaylist(3);
    });
  });
}


models.sequelize.sync({force: true}).then(function() {

    fs.readFile('./songs.json', function(err, data) {
        var music_data = JSON.parse(data);
        var song = music_data['songs'];

        song.forEach(function(song) {
            //console.log(song);
            models.Song.create({
                title: song.title,
                album: song.album,
                artist: song.artist,
                duration: song.duration,
            });
        });
    });
    fs.readFile('./playlists.json', function(err, data) {
        var music_data = JSON.parse(data);
        var playlists = music_data['playlists'];

        playlists.forEach(function(playlist) {
            //console.log(playlist);
            models.Playlist.create({name: playlist.name,}).then(function(created){
                playlist.songs.forEach(function(song){
                song++;
                models.Song.findOne(
                  {
                    where:
                      {
                        id: song
                      }
                  })
                  .then(function(song){
                    created.addSong(song);
                  })
                });
              });
            });
   });
   create_users();


});
